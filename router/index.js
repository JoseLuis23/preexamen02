import express from "express";
export const router = express.Router();
export default { router };
//Generar ruta//
router.get("/", (req, res) => {
	const params = {
		recibo: req.query.recibo,
		nombre: req.query.nombre,
		domicilio: req.query.domicilio,
		servicio: req.query.servicio,
		kw: req.query.kw
	};
	res.render("index", params);
});

router.post("/", (req, res) => {
	const params = {
		recibo: req.body.recibo,
		nombre: req.body.nombre,
		domicilio: req.body.domicilio,
		servicio: req.body.servicio,
		kw: req.body.kw
	};
	res.render("index", params);
});
